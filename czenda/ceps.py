from typing import Optional
import datetime

import lxml
import pandas as pd
import xmltodict
import zeep


class CEPS:
    """A simplified CEPS data reader"""

    WSDL_URI = "http://www.ceps.cz/_layouts/CepsData.asmx?WSDL"

    def __init__(self) -> None:
        self._client: Optional[zeep.Client] = None

    @property
    def client(self) -> zeep.Client:
        """CEPS WSDL client"""
        if self._client is None:
            self._client = zeep.Client(self.WSDL_URI)
        return self._client

    def get_generation(self, start: datetime.datetime, end: datetime.datetime) -> pd.DataFrame:
        """Read generation data"""
        raw_data = self.client.service.Generation(
            start, end, agregation="HR", function="AVG", version="RT", para1="all",
        )
        dict_data = xmltodict.parse(lxml.etree.tostring(raw_data))

        df = pd.DataFrame.from_dict(dict_data["root"]["data"]["item"])
        column_names = {
            f"@{item['@id']}": item["@name"].split()[0] for item in dict_data["root"]["series"]["serie"]
        }
        column_names["@date"] = "timestamp"
        df = df.rename(columns=column_names).set_index("timestamp")

        return df
