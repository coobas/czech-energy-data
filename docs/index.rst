.. CZENDA documentation master file, created by
   sphinx-quickstart on Sat Jan  4 22:02:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CZENDA's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ceps


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
